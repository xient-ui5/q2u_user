sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/viz/ui5/controls/common/feeds/FeedItem",
	"sap/viz/ui5/format/ChartFormatter",
	"sap/m/MessageToast"
], function(Controller, FeedItem, ChartFormatter, MessageToast) {
	"use strict";

	return Controller.extend("Q2U_FrontEndUser.controller.Chart", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf Q2U_FrontEndUser.view.Chart
		 */
		onInit: function() {
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.oRouter.getRoute("chart").attachPatternMatched(this._onObjectMatched, this);
		},
		_onObjectMatched: function(oEvent) {
			var t = this;
			var vizFrame = t.getView().byId("idVizFrame");
			vizFrame.setBusy(true);
			var oPopOver = this.getView().byId("idPopOver");
			var wid = oEvent.getParameter("arguments").wid;
			this.charttype = oEvent.getParameter("arguments").cType;
			this.jsonModel = new sap.ui.model.json.JSONModel();
			var oModel2 = new sap.ui.model.odata.ODataModel("/sap/opu/odata/XIENT/Q2U_QUERIES_ODATA2_SRV/");
			oModel2.read(
				"/ET_INITIALSet", {
					filters: [new sap.ui.model.Filter("wd_id", sap.ui.model.FilterOperator.EQ, wid)],
					urlParameters: {
						"$select": "Chavl,Chanm,ChavlExt,Caption,Zrowid,Zcolid,Value,FormattedValue,Currency,Unit"
					},
					success: function(oData) {
						for (var i = 0; i < oData.results.length; i++) {
							if (oData.results[i].Chavl === "SUMME") {
								delete oData.results[i];
							}
						}
						t.jsonModel.setData(oData);
						vizFrame.setBusy(false);
					},
					error: function(err) {
						vizFrame.setBusy(false);
						MessageToast.show("Ein Fehler ist beim laden der Daten aufgetreten.",{
							duration: 10000
						});
					}
				});
			oPopOver.connect(vizFrame.getVizUid());
			oPopOver.setFormatString({
				"Cost": ChartFormatter.DefaultPattern.STANDARDFLOAT,
				"Revenue": ChartFormatter.DefaultPattern.STANDARDFLOAT
			});

			vizFrame.setModel(t.jsonModel);
			vizFrame.setVizProperties({
				plotArea: {
					dataLabel: {
						visible: true
					},
					legend: {
						visible: true
					}
				}
			});
			vizFrame.setVizType(this.charttype);
			vizFrame.destroyFeeds();
			this.setUid(this.charttype);
		},
		setUid: function(charttype) {
			var vizFrame = this.getView().byId("idVizFrame");
			var feedValueAxis, feedCategoryAxis;
			switch (charttype) {
				case "pie":
				case "donut":
					feedValueAxis = new FeedItem({
						'uid': "size",
						'type': "Measure",
						'values': ["Value"]
					});

					feedCategoryAxis = new FeedItem({
						'uid': "color",
						'type': "Dimension",
						'values': ["Chavl"]
					});

					vizFrame.addFeed(feedValueAxis);
					vizFrame.addFeed(feedCategoryAxis);
					break;
				case "bar":
				case "column":
				case "line":
					feedValueAxis = new FeedItem({
						'uid': "valueAxis",
						'type': "Measure",
						'values': ["Value"]
					});

					feedCategoryAxis = new FeedItem({
						'uid': "categoryAxis",
						'type': "Dimension",
						'values': ["Chavl"]
					});

					vizFrame.addFeed(feedValueAxis);
					vizFrame.addFeed(feedCategoryAxis);
					break;
			}
		},
		onNavBack: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("home");
			//console.log(oRouter);

			/*			var oHistory = sap.ui.core.routing.History.getInstance(),
							sPreviousHash = oHistory.getPreviousHash();
							
						if (sPreviousHash !== undefined) {
							// The history contains a previous entry
							history.go(-1);
						}*/
		}
	});

});