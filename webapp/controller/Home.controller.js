sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("Q2U_FrontEndUser.controller.Home", {

		onInit: function() {
			this.getView().byId("widgetPanel").removeAllContent();
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			
			this.chartInfo = [];
			
			var t = this;
			var widgetModel = new sap.ui.model.json.JSONModel();
			var widgetData = {};
			widgetData.method = "ZUI5_GET_WIDGETS";
			widgetModel.loadData("/sap/bc/xient?sap-client=001", widgetData, true, "GET");
			widgetModel.attachRequestCompleted(function() {
				t.buildTiles(widgetModel.getData());
			});
		},
		buildTiles: function(data) {
			var t = this;
			var panel2 = this.getView().byId("widgetPanel");
			var tile, tileContent, icon;
			for (var i = 0; i < data.length; i++) {
				tile = new sap.m.GenericTile(this.createId("Wid_" + data[i].wdId), {
					header: data[i].name,
					subheader: data[i].description,
					press: [t.onTilePress, t]
				});

				tileContent = new sap.m.TileContent(this.createId("arrId_" + i), {
					footer: ""
				});

				icon = new sap.m.ImageContent({
					src: t.setIcon(data[i].chartType)
				});
				
				this.chartInfo.splice(data[i].wdId + 1, 0, data[i].chartType);
				
				tileContent.setContent(icon);
				tile.addStyleClass("sapUiTinyMargin");
				tile.addTileContent(tileContent);
				panel2.addContent(tile);
			}
		},
		onTilePress: function(oEvent) {
			var regEx = new RegExp("(\\d+)$");
			var regEx_forID = new RegExp('(\\d+)$', 'g');
			var reId = regEx.exec(oEvent.oSource.sId);
			var arrId = regEx_forID.exec(oEvent.oSource.mAggregations.tileContent["0"].sId);
			this.oRouter.navTo("chart", {
				wid: reId[0],
				cType: this.chartInfo[arrId[0]]
			});
		},
		setIcon: function(chartType) {
			switch (chartType) {
				case "pie":
					return "sap-icon://pie-chart";
				case "donut":
					return "sap-icon://donut-chart";
				case "bar":
					return "sap-icon://horizontal-bar-chart";
				case "column":
					return "sap-icon://bar-chart";
				case "line":
					return "sap-icon://line-chart";
			}
		}
	});
});